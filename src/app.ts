import express, {Response, Request, NextFunction} from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import router from './myRouter.js';
import dotenv from 'dotenv';

const { PORT, HOST } = process.env;
// 
//const PORT=3030;
// const HOST='localhost';
const app = express()

app.use(morgan('dev') ); 
let appMiddle = (req : Request,res: Response,next: NextFunction)=>{
    console.log("appMiddle  middleware function");
    next();
}
app.use(appMiddle); 

// let funcMiddle = (res,req,next)=>{
//     console.log("normal users function middleware function");
//     next();
// }

app.use('/router',router);

app.get('/users', (req, res) => {
    res.status(200).send('normal Get all Users')
})

app.listen(Number(PORT), HOST as string , ()=> {
// app.listen(parseInt(process.env.PORT), process.env.HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

app.use('*',  (req : Request,res: Response,next: NextFunction) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    res.status(404).json(` - 404 - url ${full_url.href+full_url.pathname} was not found BY OMRI`)
    // res.status(404).send(` - 404 - url ${req.url} was not found BY OMRI `)
})

