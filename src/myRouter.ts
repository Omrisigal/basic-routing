
import express, {Response, Request, NextFunction} from 'express';

const router = express.Router();
let routeMiddle = (req : Request,res: Response,next: NextFunction)=>{
    console.log("route middleware function");
    next();
}
router.get('/some', routeMiddle, (req : Request,res: Response,next: NextFunction) => {
    res.status(200).send('Route Get all Users')
})

router.get('/', routeMiddle, (req : Request,res: Response,next: NextFunction) => {
    res.status(200).send('Routeeeeing TYPESCRIPT')
})

export default router;